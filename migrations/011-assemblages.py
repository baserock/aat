#!/usr/bin/env python
# Copyright (C) 2016  Codethink Limited
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.

'''Migration to Baserock Definitions format version 11.

In definitions version 11, 'systems' and 'strata' are no longer ontological
primitives, opting instead for assemblages, which contain contents lists
of contents: List<Either Assemblage Chunk>

'''

import requests
import string
import logging
import re
import sys
import warnings
import migrations
import yaml


TO_VERSION = 11

def decontextualise_system(contents, filename):
  del(contents['arch'])
  contents['contents'] = contents['strata']
  del(contents['strata'])
  contents['kind'] = 'assemblage'
  return True

def decontextualise_stratum(contents, filename):
  contents['contents'] = contents['chunks']
  del(contents['chunks'])
  contents['configuration-extensions'] = []
  contents['kind'] = 'assemblage'
  return True

def decontextualise_chunk(contents, filename):
  return True

try:
    if migrations.check_definitions_version(TO_VERSION - 1):
        system_success = migrations.process_definitions(
            kinds=['system'],
            validate_cb=lambda x, y: True,
            modify_cb=decontextualise_system)
        success = migrations.process_definitions(
            kinds=['stratum'],
            validate_cb=lambda x, y: True,
            modify_cb=decontextualise_stratum)
        if not success:
            sys.stderr.write(
                "Migration failed due to one or more warnings.\n")
            sys.exit(1)
        else:
            migrations.set_definitions_version(TO_VERSION)
            sys.stderr.write("Migration completed successfully.\n")
            sys.exit(0)
    else:
        sys.stderr.write("Nothing to do.\n")
        sys.exit(0)
except RuntimeError as e:
    sys.stderr.write("Error: %s\n" % e.message)
    sys.exit(1)
