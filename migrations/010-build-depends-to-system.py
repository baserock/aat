#!/usr/bin/env python
# Copyright (C) 2016  Codethink Limited
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.

'''Migration to Baserock Definitions format version 10.

In definitions version 10, strata build dependencies are moved to the system level.
This is so that furnishing strata can be swapped out with a single modification
to the system morphology, rather requiring a complete copy of all strata
morphologies.

'''

import requests
import string
import logging
import re
import sys
import warnings
import migrations
import yaml


TO_VERSION = 10

def find_strata_by_name(system, name):
  l = list(filter(lambda x: x['name'] == name, system['strata']))
  return l[0] if len(l) > 0 else None

def find_strata_by_morph(system, morph):
  l = list(filter(lambda x: x['morph'] == morph, system['strata']))
  return l[0] if len(l) > 0 else None

def find_additional_bds_for_strata_in_system(system, strata):
  with open(strata) as f:
    text = f.read()
    stratum_yaml = yaml.load(text)
    bds = list(map(lambda x: x['morph'], stratum_yaml.get('build-depends', [])))
    extra_bds = list(filter(lambda x: not find_strata_by_morph(system, x), bds))
    return extra_bds

def create_noninclude_stratum_entry_from_filename(stratum):
  with open(stratum) as f:
    text = f.read()
    stratum_yaml = yaml.load(text)
    return { 'name': stratum_yaml['name'],
             'morph': stratum,
             'include-mode': 'build-time' }
 
def add_strata_bds_to_system(contents, filename):
  assert contents['kind'] == 'system'

  for x in contents['strata']:
    with open(x['morph']) as f:
      text = f.read()
      stratum = yaml.load(text)
      a = find_additional_bds_for_strata_in_system(contents, x['morph'])      
      b = list(map(lambda x: create_noninclude_stratum_entry_from_filename(x), a))
      contents['strata'].extend(b)
      
  for x in contents['strata']:
    with open(x['morph']) as f:
      text = f.read()
      stratum = yaml.load(text)
      x['build-depends'] = list(map(lambda x: find_strata_by_morph(contents, x['morph'])['name'], stratum.get('build-depends', [])))

  s = False
  while(not s):
    s = True
    for x in contents['strata']:
      f = x['build-depends']
      for y in x['build-depends']:
        a = find_strata_by_name(contents, y)['build-depends']
        f = sorted(list(set(f+a)))
      if f != x['build-depends']:
        s = False
        x['build-depends'] = f

  contents['strata'] = sorted(contents['strata'], key=lambda x: len(x['build-depends']))
 
  return True

def remove_bds_from_stratum(contents, filename):
  assert contents['kind'] == 'stratum'

  del contents['build-depends']
  
  return True

try:
    if migrations.check_definitions_version(TO_VERSION - 1):
        system_success = migrations.process_definitions(
            kinds=['system'],
            validate_cb=lambda x, y: True,
            modify_cb=add_strata_bds_to_system)
        success = migrations.process_definitions(
            kinds=['stratum'],
            validate_cb=lambda x, y: True,
            modify_cb=remove_bds_from_stratum)
        if not success:
            sys.stderr.write(
                "Migration failed due to one or more warnings.\n")
            sys.exit(1)
        else:
            migrations.set_definitions_version(TO_VERSION)
            sys.stderr.write("Migration completed successfully.\n")
            sys.exit(0)
    else:
        sys.stderr.write("Nothing to do.\n")
        sys.exit(0)
except RuntimeError as e:
    sys.stderr.write("Error: %s\n" % e.message)
    sys.exit(1)
