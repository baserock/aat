docopt==0.6.2
fs==2.0.0
jsonschema==2.5.1
PyYAML==3.12
mkdocs-material
mkdocs
