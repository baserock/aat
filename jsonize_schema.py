#!/usr/bin/env python
"""JSONize The Schema Files

Usage:
  jsonize_schema.py <sourcedir> <outdir>

Options:
  -h --help     Show this screen.

"""

import json
import os
import yaml
from docopt import docopt
from fs import open_fs


if __name__ == '__main__':
    arguments = docopt(__doc__, version='JSONize Schema V1.0')

    sfs = open_fs(arguments['<sourcedir>'])
    ofs = open_fs(arguments['<outdir>'])

    for x in sfs.walk.files(filter=['*.json-schema']):
      with sfs.open(x, "r+") as f, ofs.open(x, "w+") as g:
        a = yaml.safe_load(f.read())
        g.write(unicode(json.dumps(a, sort_keys=True, indent=4, separators=(',', ': '))))
